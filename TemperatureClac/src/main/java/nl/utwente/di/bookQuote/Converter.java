package nl.utwente.di.bookQuote;

public class Converter {
    double converter(String degree) {
        double result = (Integer.parseInt(degree) * 9/5) + 32;
        return result;
    }
}
